const courseModel = require("../models/course.model"); // import course model
const mongoose = require("mongoose"); // import mongoose

// khai báo các controller CRUD
// controller get all courses
const getAllCourses = async (req, res) => {
  // b1: thu thập dữ liệu (ko cần)
  // b2: validate dữ liệu
  // b3: xử lí dữ liệu 
  try {
    const result = await courseModel.find();
    return res.status(200).json({
      message: "Lấy danh sách course thành công",
      data: result
    })
  } catch (error) {
    // dùng hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
}

// controller create course
const createCourse = async (req, res) => {
  // b1: thu thập dữ liệu
  const {
    reqCourseCode,
    reqCourseName,
    reqPrice,
    reqDiscountPrice,
    reqDuration,
    reqLevel,
    reqCoverImage,
    reqTeacherName,
    reqTeacherPhoto,
    reqIsPopular,
    reqIsTrending
  } = req.body;

  // b2: validate dữ liệu
  if (!reqCourseCode) {
    return res.status(400).json({
      message: "Mã khóa học không hợp lệ"
    })
  }
  if (!reqCourseName) {
    return res.status(400).json({
      message: "Tên khóa học không hợp lệ"
    })
  }
  if (!reqPrice) {
    return res.status(400).json({
      message: "Giá thực tế không hợp lệ"
    })
  }
  if (!reqDiscountPrice) {
    return res.status(400).json({
      message: "Giá hiện tại không hợp lệ"
    })
  }
  if (!reqDuration) {
    return res.status(400).json({
      message: "Thời lượng không hợp lệ"
    })
  }
  if (!reqLevel) {
    return res.status(400).json({
      message: "Cấp độ không hợp lệ"
    })
  }
  if (!reqCoverImage) {
    return res.status(400).json({
      message: "Link ảnh khóa học không hợp lệ"
    })
  }
  if (!reqTeacherName) {
    return res.status(400).json({
      message: "Tên giáo viên không hợp lệ"
    })
  }
  if (!reqTeacherPhoto) {
    return res.status(400).json({
      message: "Link ảnh giáo viên không hợp lệ"
    })
  }
  if (reqIsPopular !== undefined && typeof reqIsPopular != "boolean") {
    return res.status(400).json({
      message: "Tính phổ biến không hợp lệ"
    })
  }
  if (reqIsTrending !== undefined && typeof reqIsTrending != "boolean") {
    return res.status(400).json({
      message: "Tính xu hướng không hợp lệ"
    })
  }

  // b3: xử lí dữ liệu
  try {
    const newCourse = {
      courseCode: reqCourseCode,
      courseName: reqCourseName,
      price: reqPrice,
      discountPrice: reqDiscountPrice,
      duration: reqDuration,
      level: reqLevel,
      coverImage: reqCoverImage,
      teacherName: reqTeacherName,
      teacherPhoto: reqTeacherPhoto,
      isPopular: reqIsPopular,
      isTrending: reqIsTrending
    }
    const result = await courseModel.create(newCourse);
    return res.status(201).json({
      message: "Tạo course thành công",
      data: result
    })
  } catch (error) {
    // dùng hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
}

// controller get course by id
const getCourseById = async (req, res) => {
  // b1: thu thập dữ liệu
  const courseid = req.params.courseid;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(courseid)) {
    return res.status(400).json({
      message: "Course ID không hợp lệ"
    })
  }
  
  // b3: xử lí dữ liệu
  try {
    const result = await courseModel.findById(courseid);
    if (result) {
      return res.status(200).json({
        message: "Lấy course theo id thành công",
        data: result
      })
    } else {
      return res.status(404).json({
        message:"Không tìm thấy code tương ứng id"
      })
    }
  } catch (error) {
    // dùng hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
}

const updateCourseById = async (req, res) => {
  // b1: thu thập dữ liệu
  const courseid = req.params.courseid;
  const {
    reqCourseCode,
    reqCourseName,
    reqPrice,
    reqDiscountPrice,
    reqDuration,
    reqLevel,
    reqCoverImage,
    reqTeacherName,
    reqTeacherPhoto,
    reqIsPopular,
    reqIsTrending
  } = req.body;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(courseid)) {
    return res.status(400).json({
      message: "Course ID không hợp lệ"
    })
  }
  if (reqCourseCode === "") {
    return res.status(400).json({
      message: "Mã khóa học không hợp lệ"
    })
  }
  if (reqCourseName === "") {
    return res.status(400).json({
      message: "Tên khóa học không hợp lệ"
    })
  }
  if (reqPrice === "") {
    return res.status(400).json({
      message: "Giá thực tế không hợp lệ"
    })
  }
  if (reqDiscountPrice === "") {
    return res.status(400).json({
      message: "Giá hiện tại không hợp lệ"
    })
  }
  if (reqDuration === "") {
    return res.status(400).json({
      message: "Thời lượng không hợp lệ"
    })
  }
  if (reqLevel === "") {
    return res.status(400).json({
      message: "Cấp độ không hợp lệ"
    })
  }
  if (reqCoverImage === "") {
    return res.status(400).json({
      message: "Link ảnh khóa học không hợp lệ"
    })
  }
  if (reqTeacherName === "") {
    return res.status(400).json({
      message: "Tên giáo viên không hợp lệ"
    })
  }
  if (reqTeacherPhoto === "") {
    return res.status(400).json({
      message: "Link ảnh giáo viên không hợp lệ"
    })
  }
  if (reqIsPopular !== undefined && typeof reqIsPopular != "boolean") {
    return res.status(400).json({
      message: "Tính phổ biến không hợp lệ"
    })
  }
  if (reqIsTrending !== undefined && typeof reqIsTrending != "boolean") {
    return res.status(400).json({
      message: "Tính xu hướng không hợp lệ"
    })
  }

  // b3: xử lí dữ liệu
  try {
    const updatedCourse = {};
    if (reqCourseCode) {
      updatedCourse.courseCode = reqCourseCode;
    }
    if (reqCourseName) {
      updatedCourse.courseName = reqCourseName;
    }
    if (reqPrice) {
      updatedCourse.price = reqPrice;
    }
    if (reqDiscountPrice) {
      updatedCourse.discountPrice = reqDiscountPrice;
    }
    if (reqDuration) {
      updatedCourse.duration = reqDuration;
    }
    if (reqLevel) {
      updatedCourse.level = reqLevel;
    }
    if (reqCoverImage) {
      updatedCourse.coverImage = reqCoverImage;
    }
    if (reqTeacherName) {
      updatedCourse.teacherName = reqTeacherName;
    }
    if (reqTeacherPhoto) {
      updatedCourse.teacherPhoto = reqTeacherPhoto;
    }
    if (reqIsPopular) {
      updatedCourse.isPopular = reqIsPopular;
    }
    if (reqIsTrending) {
      updatedCourse.isTrending = reqIsTrending;
    }
    const result = await courseModel.findByIdAndUpdate(courseid, updatedCourse);
    if (result) {
      return res.status(200).json({
        message: "Cập nhật course theo id thành công",
        data: result
      })
    } else {
      return res.status(404).json({
        message: "Không tìm thấy thấy course tương ứng id"
      })
    }
  } catch (error) {
    // dùng hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
}

// controller delete course theo id
const deleteCourseById = async (req, res) => {
  // b1: thu thập dữ liệu
  const courseid = req.params.courseid;

  // b2: validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(courseid)) {
    return res.status(400).json({
      message: "Course ID không hợp lệ"
    })
  }

  // b3: xử lí dữ liệu
  try {
    const result = await courseModel.findByIdAndDelete(courseid);
    if (result) {
      return res.status(200).json({
        message: "Xóa course theo id thành công"
      })
    } else {
      return res.status(404).json({
        message: "Không tìm thấy course tương ứng id"
      })
    }
  } catch (error) {
    // dùng các hệ thống thu thập lỗi để thu thập err
    return res.status(500).json({
      message: "Có lỗi xảy ra"
    })
  }
}

// export controllers
module.exports = {
  getAllCourses,
  createCourse,
  getCourseById,
  updateCourseById,
  deleteCourseById
}