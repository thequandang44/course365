const express = require("express"); // import express
const mongoose = require("mongoose"); // import mongoose
const path = require("path"); // import path
const app = express(); // khởi tạo app express
const port = 8000; // khai báo cổng chạy api
const cors = require('cors'); // import cors

// khai báo tạm import schema
const courseSchema = require("./app/models/course.model");
// import router
const courseRouter = require("./app/routes/course.route");

// cấu hình để dùng json
app.use(express.json());

// định nghĩa middleware phục vụ các tệp tĩnh
app.use(express.static("views"));

// middleware gắn Access-Control-Allow-Origin vào response
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// khai báo kết nói mongoDB qua mongoose
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course365")
.then(() => {
  console.log("Successfully connected !");
})
.catch((err) => {
  console.log(err.message);
});

// định nghĩa route cho trang chủ
app.get("/", (req, res) => {
  console.log(__dirname);
  const coursePath = path.join(__dirname + "/views/course365.html");
  res.sendFile(coursePath);
});

// cho phép cors với route
app.use(cors());

// router middleware 
app.use("/courses", courseRouter);

// listen
app.listen(port, () => {
  console.log(`App listening on port ${port}`);
});