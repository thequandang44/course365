const express = require("express"); // import express
const router = express.Router(); // import router
// import controllers
const {
  getAllCourses,
  createCourse,
  getCourseById,
  updateCourseById,
  deleteCourseById
} = require("../controllers/course.controller");

// các phương thức router
router.get("/", getAllCourses);
router.post("/", createCourse);
router.get("/:courseid", getCourseById);
router.put("/:courseid", updateCourseById);
router.delete("/:courseid", deleteCourseById);

// export router
module.exports = router;